package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // List to store (valid) user entered decimal numbers
        List<Double> inputShareValues = new ArrayList<>();

        double sumOfGains = 0.0;
        double sumOfLosses = 0.0;

        // Take input from user
        Scanner scanner = new Scanner(System.in);
        boolean userInputActive = true;


        int count = 1;
        while (userInputActive) {
            System.out.println("Please enter the share value for day " + count + ": ");
            String userInput = scanner.nextLine();

            // Try to convert to double
            try {
                double userDouble = Double.parseDouble(userInput);
                inputShareValues.add(userDouble);

                boolean isValidAnswer = false;

                while (!isValidAnswer) {
                    System.out.println("Add another value? (y/n)");
                    userInput = scanner.nextLine();

                    if (userInput.equalsIgnoreCase("n")) {
                        // Exit the loop
                        userInputActive = false;
                        isValidAnswer = true;
                    } else if (userInput.equalsIgnoreCase("y")) {
                        isValidAnswer = true;
                    }
                    else {
                        System.out.println("Not a valid answer!");
                    }

                }

                count++;
            }
            catch (NumberFormatException e) {
                System.out.println("Not a valid number! Please try again");
            }
        }

        // Now traverse through the input values and calculate sums
        if (inputShareValues.size() > 1) {
            for (int i = 1; i < inputShareValues.size(); i++) {

                // Check the difference to determine gain or loss
                double diff = inputShareValues.get(i) - inputShareValues.get(i-1);

                if (diff > 0) {
                    // It's gain
                    sumOfGains += diff;

                } else {
                    // It's loss
                    sumOfLosses += diff;
                }
            }

            System.out.println(String.format("Total Gains: %1$,.2f", sumOfGains));
            System.out.println(String.format("Total Losses: %1$,.2f", sumOfLosses));
        }
        else {
            System.out.println("Insufficient input data to determine gain or loss!");
        }

    }






}