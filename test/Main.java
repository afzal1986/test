package com.company;

public class Main {

    public static void main(String[] args) {

        // The assignment does not specify where the input comes from (user input from console, text file) so we assume
        // for the sake of simplicity that it's a static array
        double[] inputShareValues = {78.41, 85.18, 91.09, 90.57, 91.02, 103.61, 105.88, 103.77, 110.13, 108.89, 105.09};

        double sumOfGains = 0.0;
        double sumOfLosses = 0.0;

        if (inputShareValues.length > 1) {
            for (int i = 1; i < inputShareValues.length; i++) {

                // Check the difference to determine gain or loss
                double diff = inputShareValues[i] - inputShareValues[i-1];

                if (diff > 0) {
                    // It's gain
                    sumOfGains += diff;

                } else {
                    // It's loss
                    sumOfLosses += diff;
                }
            }

            System.out.println(String.format("Total Gains: %1$,.2f", sumOfGains));
            System.out.println(String.format("Total Losses: %1$,.2f", sumOfLosses));
        }
        else {
            System.out.println("Insufficient input data to determine gain or loss!");
        }

    }


}